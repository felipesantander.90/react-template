import logo from './logo.svg';
import './App.css';
import { useQuery, gql, useMutation } from "@apollo/client";
import { useEffect } from 'react';

const GET_TOKEN = gql`
  mutation {
    tokenAuth(username: "admin", password:"admin"){
      token
    }
}
`;

const FILMS_QUERY = gql`
  {
    sums{
      firstNumber
    }
  }
`;

function App() {
  const [mutateFunction, {data, loading, error }] = useMutation(GET_TOKEN);
  useEffect(() => {
    mutateFunction().then(({data}) => {
      if(data){
        console.log(data.tokenAuth.token);
      }
    }
    );
  }, []);

  if (loading){ 
    return "Loading..."
  };
  if (error) return <pre>{error.message}</pre>;
  return (
    <div>
      <h1>actualizado 2 </h1>
      <ul> 
        {data ? data.tokenAuth.token : "No data"}
      </ul>
    </div>
  );
}

export default App;
